---
title: blog
icon: ra ra-scroll-unfurled
index: 4
table:
- date
---
# Blog
Come tutti, nel corso degli anni, ho sviluppato un'idea personale del gioco di ruolo. Alcune idee sono condivisibili, altre meno, nonostante tutto voglio condividerle con voi.
> La verità emerge più facilmente dall'errore che dalla confusione.
> – Francis Bacon
<div data-pages></div>