---
name: Aalis Wintercrest
game system: Fabula Ultima
game type: Campagna
theme: winter
table:
- index
- title
---
## Introduzione
Questa campagna è stata giocata su Fabula Chronicles stagione 0. Similmente a Adventurers League per D&D, Chronicles è il gioco organizzato per Fabula Ultima.
Alcuni capitoli contengono personaggi diversi oltre ad Aalis Wintercrest, che era il mio personaggio, tuttavia molti personaggi sono ricorrenti poiché ho trovato un buon gruppo, quindi abbiamo pianificato la sessione per giocare insieme.

## Capitoli
<div data-pages></div>