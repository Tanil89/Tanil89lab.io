---
title: La storia di Aalis
index: 0
---
# Background
<figure class="float-left mr-5 mt-5 mb-1 w-48" alt="Ophilia Clement from Octopath Traveler 1">
    <img class="mask mask-hexagon" src="assets/AalisWintercrest.png" />
</figure>

### Il passato di Aalis

Sin da giovane, Aalis dimostrò di possedere doni magici. Essendo la terzogenita di una famiglia nobile minore, ebbe la fortuna di non essere schiacciata dagli impegni di successione e poté dedicarsi allo studio fin dalla tenera età.

Accadde che, giocando con i suoi coetanei, si facesse male e i suoi amici più piccoli si mettessero a piangere per le ferite superficiali. Così, Aalis usava la sua debole magia per curare i piccoli tagli dei suoi amici, e si rallegrava nel vedere il sorriso tornare sui loro volti. Fu in quel momento che decise di dedicarsi a lenire le ferite altrui.

In particolare, creò un forte legame con Frederic Cortes, un aspirante cavaliere, spericolato come pochi e sempre in cerca di avventure. Aalis e i suoi amici venivano spesso rimproverati per le marachelle che combinavano insieme. La famiglia di Frederic, una casata di cavalieri, era devota nell'assistere e proteggere la famiglia Wintercrest.

Crescendo, Aalis si dedicò agli studi, alla retorica necessaria per il suo rango nobiliare e alla religione nella Chiesa delle Lune. Questo fu un periodo piuttosto difficile, ma le permise di acquisire le conoscenze necessarie per aiutare gli altri.

Una volta terminati gli studi, Aalis tornò a casa e incontrò il suo amico durante una pattuglia. L’addestramento lo aveva reso più serio e affidabile. Lei gli raccontò degli attacchi recenti di mostri nei villaggi periferici e lo convinse a farla entrare nell’Ordine d’Avorio per proteggere i deboli. Frederic conosceva il temperamento di Aalis e sapeva che era il posto giusto per lei.

Aalis ha vissuto molte avventure fino ad ora. Da cadetta e guaritrice, non si è mai trovata in reale pericolo, essendo sempre protetta da Cortes, tranne una volta. 

Durante una missione per indagare su un mostro, si trovarono davanti a un demone. Per permettere ad Aalis di fuggire, Frederic affrontò da solo la creatura. Dopo quell’evento, il rapporto tra Aalis e Cortes si raffreddò, e Frederic chiese di essere esonerato dai suoi doveri verso la famiglia Wintercrest.

Separatosi dal suo amico di infanzia, Aalis si dedicò interamente al suo lavoro per l'ordine di Avorio, sperando, un giorno di rincontrare nuovamente il suo amico di infanzia.