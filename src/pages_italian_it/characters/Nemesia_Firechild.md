---
name: Nemesia Firechild
game system: Goblin Slayer TTRPG
game type: One-shot
---
Questa one-shot è stata realizzata per testare il manuale del Goblin Slayer TTRPG, che sarebbe stato rilasciato ufficialmente dopo 2 settimane.
La maggior parte dei giocatori non conosceva le regole, quindi questa avventura è stata progettata per essere facile da giocare in circa 2 ore. Nessuno aveva un background, poiché si trattava di una one-shot.
Io ho interpretato Nemesia Firechild, una sacerdotessa del Dio Supremo.
Ecco la lista dei personaggi dei giocatori.
<div data-pages></div>