---
title: Personaggi
index: 2
theme: night
icon: ra ra-player-pyromaniac
table:
- name
- game system
- game type
---
# Selezione dei personaggi
Qui potete trovare una lista di personaggi che ho giocato in passato.
Non voglio annoiarvi con le meccaniche di gioco, non credo che questo sia il fulcro del gioco di ruolo.
Cercherò di raccontarvi la storia dei personaggi che ho interpretato, dal loro POV. Potrete leggere un racconto, spero che vi piaccia. Non sono uno scrittore.

<div data-pages></div>