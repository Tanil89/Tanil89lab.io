---
title: Il seme della discordia
game system: Goblin Slayer TTRPG
game type: 2D
date: 2024-05-06T22:00:00+01:00
---
# Il seme della discordia
## Characters
* **Nemesia Firechild (Sacerdotessa):** È una sacerdotessa del Dio Supremo.
Può usare i suoi poteri per 3 volte al giorno. 
I suoi miracoli si concentrano sulla guarigione (può guarire le ferite e le malattie), può bandire una scuola di incantesimi su un nemico per un breve periodo e può comprendere la natura di un oggetto.
* **Nuvus Dita di Polvere** Nano sciamano, può invocare le arti spiritiche 3 volte al giorno. I suoi incantesimi comprendono attacchi basati sulle fiamme come saette di fuoco oppure ondate di calore.
* **Mimosa Bilberry** Arciera elfica. Giovane per la sua razza (1000 anni) è specializzata nel combattimento a distanza con l'arco.
* **Mavis Soliana** Giovane ragazza Rhea, unico personaggio multiclasse combattente/esploratore. Specializzata nel combattimento con la spada leggera.
* **Trirat** Monaco lucertoloide. Combatte senz'armi, indossa un'armatura leggera ed è particolarmente resistente.
## La convocazione