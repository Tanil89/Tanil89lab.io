---
title: todo
index: 5
icon: ra ra-player-dodge
---
### Next Tasks:
1. componente infobox nell'autoindex da meta.table
1. mettere dove servono gli attributi aria-hidden, aria-label, aria-labelledby
1. usare breakpoint sm/md/lg più grande per far comparire il testo nei tasti della navbar
1. prenire Flash Of Unstyled Content
1. OPZ ricerca all'interno delle pagine
1. fixare i bottoni quando il nome è troppo lungo
1. rilevare la lingua del browser e la prima volta andare sulla versione giusta (italiano o inglese)
### Completed:
1. fix Pages CI
1. temi nel file di config json come semplice array di stringhe, autocapitalizzarli per stamparli nel menu, e passati per un Set per rimuovere duplicati WONTFIX
1. urlencode id header nel toc in sidebar
1. inglese
    1. ~~tasto per cambiare lingua a fianco al cambio tema~~
    1. autogenerato da cartelle src/pages_italian_it che risultano in copie dei file in dist/home.md dist/it/home.md e versioni alternative disponibili d'ogni pagina 
    1. salvate come array di stringhe in meta, da leggere ogni volta sull'await
1. normalizzare i tasti di sidebar e tema (btn-ghost text-xl, rimuovere scritta sul tema)
1. Nel theme picker Dynamic in maiuscolo per distinguerlo dai temi specifici
1. generatore di sitemap standard e feed RSS
1. ordinare con l'index la lista dei file
1. FIX: aggiorna sitemap col dominio gitlab (metterlo nel file di configurazione insieme a temi e tutti gli altri metadati)
    1. fix lettura sitemap
1. app separata: generatore statico di route da una cartella di file markdown, con meta da header YAML
1. ricerca e navigazione rapida
1. tag opengraph
1. `"use strict";` sul codice
1. tasti rapidi nella navbar da index al json (quickLinks)
1. sidebar sempre visibile quando lo schermo è largo
1. id interni delle sezioni della pagina in side/navbar, come indice di navigazione (magari anche come altra sidebar a destra)
1. temi diponibili nel selector dal JSON
1. icone in giro, definite nel JSON (come nomi classi di un'icon font, Remix o Bootstrap)
1. pagina successiva / precedente
1. supporto markdown con HTML opzionale per gli articoli
1. spostare autoindex da pagina separata a modulo universale, inseribile in altre pagine con un solo elemento senza script
1. Provare a leggere meta.title || meta.name per le voci in navbar e breadcrumbs
1. sorting e formato ora sull'autoindex
1. FIX: rimuovere underscore tra parentesi nella ricerca