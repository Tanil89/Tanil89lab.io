---
title: blog
icon: ra ra-scroll-unfurled
index: 4
table:
- date
---
# Blog
Like everyone, over the years, I have developed a personal ideas about role-playing. Some ideas are agreeable, some less so, nonetheless I want to share them with you.
> Truth emerges more readily from error than from confusion.
> – Francis Bacon
<div data-pages></div>