---
title: The importance of rules
author: Tanil
date: 2024-05-14T11:20:00+01:00
---
# The Importance of Rules
## Why Do We Need Rules?
> The Game Master is that friend who, to make you feel good, lets you enter his dreams and gives you permission to leave a mark there.

Role-playing games are like this: the Game Master invents a world and leaves the freedom to the players to shape it to write a story together with them. This, on his part, is an act of trust. So, why do we need rules?

A story where no obstacles are encountered is not interesting to narrate, but how to manage these obstacles?

Sure, the Game Master could arbitrarily decide the outcome of events. This is what actually happens in "free form", where the Game Master decides every aspect of the game. How to decide if the bandit's sword hit a vital point of the player or if the blow was not fatal? It seems like a trivial decision, but it burdens the Game Master with a lot of responsibilities. If he decides to spare the player, he risks taking away all the challenge and pathos from the game. I have seen many Game Masters spare the players because they did not want that character to die, but nonetheless, it's a mistake.

The problem also happens the other way around: seeing a character you had grown fond of die, when the master had the power to save him, can create rancor within the game group. This, on the other hand, is the player's mistake. The master should be an impartial arbiter in his own story.

This is where the need for rules comes in. These relieve the Game Master from the responsibility of deciding on dubious, leaving the burden to the rules.
## When to use the rules?
Now I'll talk in general terms, since every player's handbook is different. However, there are some general rules that can be applied to every handbook. The test should be used when the result of an action is **doubtful**, or it is interesting to consider the **degree of success**. Let's illutrate this with some examples:

Let's figure that we are playing in a setting where the magic exists and all can use it. The wizard wants to cast a cantrip, a low level spell that everyone knows. Does it make sense for him to do a test? Does it make sense he could fail? No, because that action is simple for him, and no one is distracting him.  
Now the warrior wants to do the same action, having seen it performed by the wizard. Now the master has two choices: He could deny him the test since is not a skill he can perform without training, or he could allow him to try it with a higher difficulty (and a worse, clumsier effect of the magic ) if he feels it might be important to the plot.  
For istance,  to allow to the character to start a multiclass or to introduce new plot elements.
The player could use the opportunity of a good test to gain a mage level, or the master could introduce a new way into the story, a previously unknown way to learn magic in an unconventional way.

Another example is a study test. Take, for example, that the scholar in the group comes from the capital city and asks for a study test on the city's main monument. Does it make sense for him to fail? No, because study is his character's strong point; moreover, it is common knowledge for his background. In this case, the test is not necessary **for that specific character**. However, in this case, the Game Master may require a test not to give the information, but to add additional information. Even with a critical failure, the player would have the same information as a normal player would have without his knowledge.

Usually combat is always considered a dubious case, as the stakes are higher; however, distinctions must be made here as well.
I remember when I had first started playing, in my second session of D&D 4e, there was the last enemy, a prone goblin in the swamp with 2 life points left. The only player in range had to wait his turn to attack, failing to attack on an enemy that was practically begging to die.  
Moral of the story, we spent almost 3 turns just behind that goblin because of the dice.  
In hindsight, did it make sense to continue that fight? No, because there was no longer a degree of challenge, no more narrative interest, no more reason to follow the rules. It was simply enough to stop the confrontation and decide at the table how to end it.

What I think needs to be avoided is turning the game into continuous tests, each player should choose the specific approach, although it may be comical to see the ignorant warrior pass a study test on a subject he should know nothing about, it can be debasing to the characters who are focused on that aspect of the game.
## The rules from the player's point of view
In my opinion, the rules are all the more important the more inexperienced the player is. I have often played with novice players and have noticed that many of them get stuck not knowing what they can or cannot do.
It is comfortable for these players to have a codified set of actions until they become familiar with the game.
Thereafter, these can freely think outside the box and stop thinking about “what they _can_ do” to “what they _want_ to do.”

This does not mean that the experienced player ignores the rules; the rules are the skeleton on which the game is based, but if an action is not codified, it does not mean that it cannot be done. The player is free to present his or her idea to the GM, who will decide, based on the rules, whether it is possible and how to adapt the rules for that purpose. Do not limit imagination.
## The GM and the rules
Are rules impediments or valuable tools for a GM? It depends.  
The GM's job is to referee the game; this means that in addition to interacting with their NPCs, the master interacts with the player through the rules.  
I have heard some GMs feel so burdened by the rules that they seek only very narrative regulations. I am reminded by a good friend of mine that if a rulebook has one rule, this one has one rule too many. For me it is the opposite: I love well-structured regulations that include many instances.

The regulations allow a lot of responsibility to be relieved from the GM's point of view. In the preparatory phase, the GM predicts the main tests with possible consequences and confrontations; then what will happen is no longer in his or her power. This depends on the players' choices, including consequences, such as the death of a character.
I mentioned that tests are used to define consequences, but a crafty GM could use a test to create the live plot!
Imagine being able to exploit a critic on an unexpected test to reveal an important plot character early, or to reveal new talents. This may seem insignificant, but for the player it is an exhilarating thing that can even prompt the player to change his or her character's growth.
I remember making a critical success in a study test-the master revealed important and compromising information about an enemy that had not yet been introduced. This prompted the group to investigate him, actually coming more prepared for the confrontation. The master confessed that he did not foresee this but exploited it. Players' actions are important and should not be overlooked.

### Can the GM break the rules?
As with the player, the GM must remember that the rules are a guide. But can the GM cheat?
Yes, but he must be very careful. As I said before, the rulebook protects the Game Master, but if he decides to violate it, he does so at his own peril! Here are some things to keep in mind:
* **The whole group must be aware and in agreement.**  
Remember that the rulebook is an anchor for the player; he builds strategies and builds on it. Buying useless skills because the master ignores them can be frustrating.
* **It does not have to be a constant**  
It is okay to “cheat” sometimes, but it must be situational and motivated.
* **It should not be done for someone's benefit**.  
These gimmicks must be done for plot and atmosphere, not on a strategic choice.

When you want to bend a rule, think, “Why am I doing this? Would I be able to explain it to the player after the campaign is over?”  
I remember once denying a spell with a successful spell test to a player. The reason was that an NPC present in the scene prevented it. The player was unaware of the NPC's presence, so I did not make him take a perception test; I secretly did it for him.  
Could I have simply told the truth about the counter spell? Sure, but I felt that narratively it was more interesting to say, “Something prevented you from casting the spell.” We were playing on a rulebook where all tests are public.

The Game Master always has the final say; it is a big opportunity but also a big responsibility, use it well!
## Types of regulations.
Regulations in role-playing games can vary widely in approach, ranging from the more narrative to the more mechanical.

Systems such as _Dungeons & Dragons_ take an approach similar to a board game, with detailed regulations that analyze every aspect of the game. At high levels, a test may have to account for many modifiers. This precision in testing can reduce the fluidity of the narrative. Combats are handled on a dashboard, providing a tactical aspect but requiring a lot of work for the GM.

Systems such as _Fabula Ultima_ or _Ryuutama_ use more minimal and versatile regulations, where combat can be tactical even without a board. However, the low specificity of tests, based only on statistics without modifiers, can reduce character customization, as the same statistic can be used for tests of study, perception, or orientation.

Some rulebooks are specifically narrative, such as _Not the End_ (which I have not tried). In this game, narrative is key, and tests introduce positive and negative aspects based on extracted successes and failures. This provides more narrative freedom, but some may feel forced to introduce narrative aspects just because they have been extracted, ridiculing the story.

### Punitive or permissive regulation?
This is not directly related to the mechanical aspect, but it deserves analysis. Every rulebook introduces difficulties, but some are very punitive, where every fight can be lethal.

In _Sine Requie_, fights are very dangerous even for high-level characters, prompting players to avoid the fight. I remember that due to a failure, my character, despite being at an advanced level of the story, was about to die against a basic undead. This keeps tension high in even the simplest fights, but can cause unseemly deaths.

_Fabula Ultima_ aims for less punishing fights; the character cannot die unless he or she decides to sacrifice. This allows them to experience defeat without reducing pathos.

When choosing the game, consider this aspect as well.
## Conclusion.
Role-playing is a free experience and should remain so. I can only recommend that you carefully choose together a rulebook that lends itself well to the experience you want to engage in, so that you modify it as little as possible.  
There is not only D&D, there are games that are more narrative, more mechanical, more or less punishing, etc. Try, experiment and choose.

Personally, I prefer games without a board. I feel that making maps is an exaggerated amount of work as a cost/outcome. Also, I perceive a lot of combat as an interruption to the flow of the narrative, so I look for systems to make those exciting as well.  
How good is it to have a system without an armor class, with opposing tests? I admit they are slower, but until the GM's narration you never know what has happened, always remaining in eager anticipation.

What are you still doing here? Warm up the dice!