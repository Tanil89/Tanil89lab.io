---
title: How to write a Background
author: Tanil
date: 2025-01-15T18:20:00+01:00
---
# How to write a Background
## Why Write a Background?
The usefulness of writing a background is highly personal, and not everyone uses the same approach. Later, I will analyze how to write a background, but the main thing is to understand "who are you writing the background for?"  
Some will confidently answer, "The GM told me to write it," but they would be wrong. The GM doesn't need the background to create a campaign, they might only be interested in a couple of demographic details. The background is written by and for the player.  
When creating our character, we shouldn't just think about their personality and appearance; it can be useful to ask, "What event in their life led them to become who they are?"  
I remember one of my old characters: although disillusioned with the church, he was deeply convinced that everything that happened was, in some way, a sign from God.  
This was justified by his background: he had been raised as a soldier in a corrupt religious cult. This experience had made him lose trust in the institution, but his mysterious survival (justified in-game by a high Luck score) made him see the event as a divine sign.  
These are small details, but they are important. Thanks to these two simple pieces of information, I was able to:  
- Define the character's relationship with religious institutions
- Define how the character justifies the inexplicable things they encounter
- Contextualize a purely numerical value on the character sheet.

The background allows the player to define, in advance, the worldview, ideas, and especially what led the character to see the world in this way. It is a powerful, fun, and introspective tool.  
Now that we understand why the background is useful, let's see how to structure it effectively.
## Types of Backgrounds
### Basic Information
The background can be more or less long, but we shouldn't overdo it. The story should be played out at the table, not written down before we even start. Here are some pieces of information that must be present in the background:
* General information: name, age, sex, and possibly appearance.
* Personality: the character's general nature.
* Motivation: why the character acts.

Optional, useful information includes their history and origins. I recommend preparing the names of the parents and family members in case they need to be referenced during the story.  
Now I can already imagine what you're thinking: "But didn't you say that the background defines the story, and now you're saying it's optional?"  
You're right to think that. However, the main thing is defining the character: the past story is just a tool to build their worldview. If you don't have time to write it all, you can focus on outlining the personality and motivations without worrying too much about their origins.  
Now, let's get into the details of the types.

### Guided Questions Approach
I have a controversial relationship with this method, but I must admit that it can be extremely useful, especially for new players.
#### Advantages
This approach is particularly suited to game systems where the manual integrates the setting with the rules. These systems have the undeniable advantage of posing relevant and well-contextualized questions, helping players break the ice and immerse themselves in the game world. It’s a technique that facilitates entry even for those with little experience in creating backgrounds.
#### Challenges
However, this method can lead to some small problems. Some players might feel trapped by the questions, especially if the answers have a direct impact on the game mechanics. I've found myself in situations where the proposed questions didn't reflect my vision of the character, but I was still forced to answer to comply with the rules. We're talking about a role-playing game: there should never be an impression of being limited by overly rigid rules.
#### A Tip for Everyone
Despite these challenges, I recommend even more experienced players take a look at this method. The questions can always be a useful tool for inspiration and discovering new narrative elements. With a bit of flexibility, they can enrich even the most complex and elaborate characters.
### Only Basic Information
These are minimal backgrounds where only essential information is given, and sometimes not even that.
This type of background is based on the idea that the character takes shape during the game, so there’s no need to define it in advance.  
This approach requires some improvisation skills and is best suited for players with some experience. 
#### Advantages
It allows you to dive straight into the game without needing to agree on many details in advance, focusing freely on the character sheet.  
It lets you adapt quickly to the mood of the session or what the party needs.  
An experienced player might choose to make the character more or less extroverted depending on the companions they play with.  
Especially new players might need someone to take the reins or give them space. A non-predefined character approach allows more freedom in the metagame.
#### Challenges
This system isn’t immune to challenges, mainly leading to initial stalls or inconsistencies in the first session when the player still hasn't decided how to set up the character.  
Secondly, there’s the risk of always playing the same character. Without a preset structure, a standard character tends to emerge instinctively, entering our comfort zone, which reduces variety.  
#### A Tip for Everyone
I don't recommend this method for new players, and I generally discourage it. It may be useful for one-shot games or improvised sessions.
### Keyphrase
I discovered this approach while writing this article, and it seems quite interesting.  
The approach involves defining 3-4 generic phrases during character creation that define the character’s background, such as *I have a hidden daughter* or *I made a pact with an entity*.  
These game elements can be brought up during the game and expanded upon (with the party's consent), such as defining the entity of the pact or the identity of the daughter through flashbacks.
#### Advantages
The advantages are undeniable: speed of writing and versatility during gameplay.  
By having a pact defined in advance, it allows the background to have a central role in the story rather than a marginal one, without appearing as a Deux ex Machina.
#### Challenges
It may not fully fulfill the background’s task of characterizing the character, resulting in a rather generic character.  
Narrative elements can still be added later, and there are rule systems that explicitly allow this. Additionally, since the background is written **for the player**, they are free to expand and deepen it at any time.
#### A Tip for Everyone
It can be an interesting method, but since it may have mechanical effects on the game, it’s advisable to discuss it beforehand and decide whether or not to use it as a group. It’s also important to consider it in relation to the type of rule system and the style of the game.
### Narrative Background
We’ve come to the elephant in the room.  
A beautifully extended background, full of references to the setting, where there are no shadows in the character’s life, everything is described with precision and detail. A nice 5-6 page background that, sadly, no one but the author will take the time to read.
#### Advantages
With this approach, we would have an extremely detailed and multifaceted character, knowing in advance their past and, in a relatively simple way, being able to predict their reactions and worldview.  
A character created this way will rarely resemble previous characters, and such a detailed guideline allows for greater consistency over the long term.
#### Challenges
Unfortunately, the GM usually doesn't have time to manage such long backgrounds, so most of the time, they will ignore them. Don’t take it personally if the GM doesn’t reference what you’ve written.  
It requires a lot of effort from the player to write the background. This leads to a sneaky and hidden challenge: not wanting to change the character's mindset.  
Instinctively, we are inclined to protect the time spent on a task, so we tend to avoid evolving the character after the events, fearing it will deviate from the initial vision. This is the antithesis of role-playing.
#### A Tip for Everyone
The narrative background can be a fascinating creative exercise, but it’s important to balance the richness of the background with the flexibility needed for the game. Leaving some elements open or undefined can be a great way to let the character evolve in response to events and other party members.
## Common Mistakes
#### Too Long Background
An overly long background will be ignored by the GM and will take too much time to write.  
Remember that the background is a starting point, not the end. Leave some gaps to be filled in during the story; the background is not written in stone, and we can expand on it later.  
If we were to write our own life story, we certainly wouldn’t narrate every detail, year by year. We’d only mention the most important things that have shaped our life.
#### Over-the-Top Background
"My character was an archmage who fought in the great war 1000 years ago..." and now, for some reason, they're a level 4 mage.  
Let’s be realistic, what’s the point of having a character who was once a hero but is now an ordinary person? To fulfill their ego or for compensation?  
When creating your character, focus on their vices, weaknesses, and dark aspects. These elements will make them more interesting and consistent with the narrative.  
I don't want to limit your creativity, but think about it: what narrative element can I or my GM use to help my character grow?
#### Little Communication with the GM
The background should always be given to the GM in advance and validated by them.  
This helps maintain narrative consistency and justifies its inclusion in the story.  
A simple example is a *Sine Requie* campaign, where occult powers are extremely rare, and the GM decided to limit their number to preserve the integrity of the setting. 
Also, remember that the GM protects the setting and the plot: an inconsistent background can only be accepted with their consent. The final decision is always up to the GM, not the player.
## My Approach
As always, the truth lies in the middle. My approach combines schematic and narrative elements of the background.  
I start by listing the character’s fundamental information as if filling out a preprinted form.  
Then I briefly describe the essential elements of their life, without going into excessive detail.  
Finally, I literally write two lines about the character’s personality.  
All this should fit on a single page.  
At this point, I pick a significant event from the character's life and **tell** it from the character’s point of view in detail. In this version, I highlight the character’s thoughts and way of speaking, as if doing a solo zero-session with the character. This part has no predefined length, but the most important thing is that it should be completely optional, containing nothing beyond what is on the first page.
#### Advantages
Since the story is optional, the GM can ignore it or read it for pure enjoyment.  
Being a story, it can focus on style, as it doesn’t need to include gameplay aspects.  
It allows you to focus on interpretive aspects that are usually not addressed in backgrounds, such as the way the character thinks and speaks.  
It’s more enjoyable to read and write.
#### Challenges
It takes more time to write.
## Conclusione
The background is your own creation, and the perfect background doesn't exist. Experiment and find what makes your character alive and unique because the journey is more important than the starting point.