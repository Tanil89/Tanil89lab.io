---
title: "Welcome"
icon: ra ra-castle-flag
index: 1
---
# Benvenuto
Ciao amico, benvenuto nel mio sito web!
Faccio giochi di ruolo da più di 17 anni, da quando avevo 16 anni. Ho deciso di condividere le mie esperienze di gioco di ruolo con gli altri mentre sto imparando un po' di HTML :)

Quindi, in questo blog troverai i miei personaggi, con i loro background, e le loro sessioni raccontate dal loro punto di vista.
Potrei raccontare le mie sessioni come gamemaster, nel qual caso le racconterò senza alcun punto di vista specifico.

Se avrò abbastanza tempo, potrei aggiungere le mie altre esperienze di gioco di ruolo.

Suggerisco di utilizzare il tema predefinito del sito, poiché potrei selezionare un tema specifico per ciascun personaggio, tuttavia sei libero di selezionare quello che preferisci.

# Chi sono
Ho iniziato a fare giochi di ruolo con amici della scuola. Ricordo che abbiamo iniziato con DnD 4e. Anche se non abbiamo apprezzato il sistema, forse la nostra inesperienza non ha aiutato; nessuno di noi aveva mai giocato prima, ma è stata una bella esperienza.

Da allora, ho provato molti sistemi: Sine Requie, Dark Heresy, free form, Warhammer Fantasy, diversi sistemi con regole casalinghe aggiuntive, Fabula Ultima, ecc.

Ho anche giocato a qualche LARP (Live Action RolePlay), a volte come giocatore, a volte come NPC. Onestamente, preferisco il ruolo di NPC, puoi interpretare molti personaggi diversi.
Attualmente sto lavorando a un progetto LARP come collaboratore, seguendo principalmente l'evento come arbitro, con un focus sulla sicurezza dei combattimenti e la gestione dei nemici.

### Links
[L'importanza delle regole](#!blog/The_importance_of_rules)