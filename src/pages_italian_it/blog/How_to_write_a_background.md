---
title: Come scrivere un Background
author: Tanil
date: 2025-01-15T18:20:00+01:00
---
# Come scrivere un Background
## Perchè scrivere un Background?
L'utilità di scrivere un background è molto personale e non tutti usano lo stesso approccio. Più avanti analizzerò come scrivere un background ma la cosa principale è capire "per chi si sta scrivendo un background?"  
Alcuni risponderanno convinti "Il master mi ha detto di scriverlo" ma sbaglierebbero. Al master non serve avere il background per creare una campagna, al limite potrebbe essere interessato giusto a due informazioni anagrafiche. Il background viene scritto dal e per il giocatore.  
Quando creiamo il nostro personaggio non dobbiamo solo pensare al suo carattere e al suo aspetto, può tornare utile pensare "quale avvenimento della sua vita l'ha portato a diventare quello che è?"  
Ricordo un mio vecchio personaggio: sebbene disilluso dalla chiesa, era profondamente convinto che tutto ciò che accade fosse, in qualche modo, un segno di Dio.  
Questo era giustificato dal background: era stato cresciuto come soldato in una setta religiosa corrotta. Quest'esperienza lo aveva portato a perdere fiducia nell'istituzione, ma la sua misteriosa sopravvivenza (giustificata in gioco da un alto punteggio in Fortuna) gli fece vedere l'evento come un segno divino.  
Sono piccole cose ma sono importanti, ma grazie a queste due semplici informazioni, sono riuscito a:
- Definire il rapporto del personaggio con le istituzioni religiose
- Definire un modo del personaggio di giustificare le cose inspiegabili che si troverà ad affrontare
- Contestuallizzare in gioco un valore puramente numerico della scheda.

Il background permette al giocatore di definire in anticipo la visione del mondo, le idee del personaggio e soprattutto cosa l'ha portato a vedere il mondo in questo modo. È uno strumento potentissimo, divertente e molto introspettivo.
Ora che abbiamo compreso perché il background è utile, vediamo come strutturarlo in modo efficace.

## Tipologie di Background
### Informazioni basilari
Il background può essere più o meno lungo, ma non dobbiamo esagerare. La storia la dobbiamo giocare al tavolo, non dobbiamo scrivere un libro ancora prima di iniziare. Ecco alcune informazioni che devono essere presenti sul background:
* Generalità: nome, età, sesso, possibilmente aspetto.
* Personalità: il carattere in generale.
* Motivazione: perché il personaggio agisce.

Informazioni opzionali utili sono la sua storia e le origini, consiglio di prepararsi già i nomi dei genitori e della famiglia, qualora durante la storia servisse citarli.  
Ora già immagino cosa starete pensando "Ma come ha detto che il background serve definire la storia ed ora dice che è un aspetto opzionale".  
Avete ragione a pensarlo. Tuttavia, la cosa principale è definire il personaggio: la storia pregressa è solo uno strumento per costruire la sua visione del mondo. Se non avete tempo di scrivere tutto, potete concentrarvi sul delineare la personalità e le motivazioni senza preoccuparvi troppo delle sue origini.
Ora entriamo nel dettaglio sulle tipologie.

### A domande guida 
Ho un rapporto controverso con questa tipologia di stesura, ma devo riconoscere che può essere un metodo estremamente utile, soprattutto per i nuovi giocatori.
#### I vantaggi
Questo approccio si presta particolarmente a sistemi di gioco in cui il manuale integra l’ambientazione con il regolamento. Questi sistemi hanno l’indubbio vantaggio di porre domande pertinenti e ben contestualizzate, aiutando i giocatori a rompere il ghiaccio e a immergersi nel mondo di gioco. È una tecnica che facilita l’ingresso anche a chi non ha molta esperienza nella costruzione di background.
#### Le criticità
Tuttavia, questo metodo può portare a qualche piccolo problema. Alcuni giocatori potrebbero sentirsi ingabbiati dalle domande, soprattutto se le risposte hanno un impatto diretto sulle meccaniche di gioco. Mi è capitato, ad esempio, di trovarmi in situazioni in cui le domande proposte non rispecchiavano la mia visione del personaggio, ma ero comunque costretto a rispondere per rispettare il regolamento. Parliamo di un gioco di ruolo: non dovrebbe mai esserci l’impressione di essere limitati da regole troppo rigide.
#### Un consiglio per tutti
Nonostante queste criticità, consiglio anche ai giocatori più esperti di dare un’occhiata a questo metodo. Le domande possono sempre essere uno strumento utile per trarre ispirazione e per scoprire nuovi spunti narrativi. Con un pizzico di flessibilità, possono arricchire anche i personaggi più complessi e articolati.
### Solo informazioni fondamentali
Sono background minimali in cui si dicono solo le informazioni essenziali e certe volte nemmeno quelle.
Questo tipo di background nasce con l’idea che il personaggio prenda forma durante il gioco, quindi non c'è necessità di definirlo in anticipo.  
Questo approccio richiede una certa capacità di improvvisazione e conviene avere una certa esperienza del gioco.  
#### Vantaggi
Permette di entrare subito nel vivo del gioco, senza dover concordare molti dettagli in anticipo e concentrandosi liberamente sulla scheda.  
Permette di adeguarsi rapidamente al clima della sessione o a quello che serve al party.  
Effettivamente un giocatore esperto potrebbe decidere di rendere il personaggio più o meno estroverso in relazione ai propri compagni di gioco.  
Soprattutto i nuovi giocatori potrebbero avere bisogno di qualcuno che prende le redini, o che gli lasci spazio. Un approccio di personaggio non preimpostato permette più libertà in metagame.
#### Criticità
Questo sistema non è immune a criticità, in primis uno stallo iniziale o incoerenze nella prima sessione, quando il giocatore deve ancora decidere come impostare il personaggio.  
In secondo luogo si rischia di avere sempre lo stesso personaggio, in assenza di un preimpostazione, istintivamente emerge un personaggio standard che entra nella nostra comfort zone, questo riduce la varietà.  
#### Un consiglio per tutti
Sconsiglio il metodo ai nuovi giocatori e lo sconsiglio in generale. Può essere utile a Oneshot o sessioni improvvisate.
### Keyphrase
Questo approccio l'ho scoperto mentre scrivevo questo articolo e sembra abbastanza interessante.  
L'approccio si basa si definire in fase di creazione del personaggio 3-4 frasi generiche che definisco il proprio BG, ad esempio *Ho una figlia nascosta* oppure *Ho fatto un patto con un’entità*.  
Questi aspetti di gioco possono venire tirati fuori durante il gioco ed ampliati (con il consenso del party) ad esempio definendo l'entità del patto o l'identità della figlia tramite flashback.
#### Vantaggi
I vantaggi sono indubbi, velocità di scrittura e versatilità durante il gioco.  
Il fatto di aver definito in anticipo la presenza di un patto, permette di collegare il patto al tono della storia permettendo al BG di avere un ruolo centrale e non marginale, senza farlo apparire un Deux ex Machina.
#### Criticità
Potrebbe non svolgere appieno il compito del BG di caratterizzare il personaggio, creando un PG abbastanza generico.  
Gli elementi narrativi possono essere comunque aggiunti in seguito, ci sono regolamenti che lo permettono esplicitamente, inoltre, dato che il BG è una cosa che viene scritta **per il giocatore**, questi ha la liberta di approfondirlo e ampliarlo in qualsiasi momento.
#### Un consiglio per tutti
Può essere un metodo interessante, tuttavia dato che può avere effetti meccanici sul gioco, vi conviene parlarne in anticipo e definire se usarlo tutti o meno. Può essere importante considerarlo anche con il tipo di regolamento e lo stile del gioco.
### Background narrativo
Siamo arrivati all'elefante nella stanza.
Un bel BG esteso pieno di riferimenti all'ambientazione nel quale non ci sono ombre nella vita del personaggio, tutto è descritto con minuzia e dettagli. Un bel BG da 5-6 pagine che, triste da dire, nessuno tranne l'autore si prenderà la briga di leggere.
#### Vantaggi
Con questo approccio avremmo un personaggio estremamente dettagliato e sfaccettato, sappiamo in anticipo i suoi trascorsi e in un modo relativamente semplice possiamo ipotizzare le sue reazioni e la sua visione del mondo.  
Difficilmente un personaggio creato in questo modo sarà uguale a personaggi precedenti e una linea guida così dettagliata permette una maggiore coerenza nel lungo termine.
#### Criticità
Purtroppo, solitamente il master non ha il tempo di gestire background così lunghi, quindi la maggior parte delle volte li ignorerà. Quindi non dovrete prendervela con il narratore se non userà riferimenti con quanto scritto.  
Richiede molto impegno da parte del giocatore per scrivere il BG. Questo porta ad una criticità subdola e nascosta, non voler cambiare la mentalità del personaggio.  
Istintivamente siamo portati a proteggere il tempo speso dietro un lavoro, per cui si tende a non fare evolvere il personaggio a seguito degli eventi per non allontanarsi alla visione iniziale del personaggio. Questo è l'antitesi del gioco di ruolo.
#### Un consiglio per tutti
Il background narrativo può essere un esercizio creativo affascinante, ma è importante bilanciare la ricchezza del BG con la flessibilità necessaria al gioco. Lasciare qualche elemento aperto o non definito può essere un ottimo modo per permettere al personaggio di evolvere in risposta agli eventi e agli altri membri del party.
## Errori comuni
Ecco qualche errore comune:
#### Background troppo lungo
Un background troppo lungo verrebbe ignorato dal master e richiederebbe troppo tempo per essere scritto.  
Dovete ricordare che il BG è il punto di partenza non di arrivo. Lasciate punti oscuri da riempire durante la storia, il BG non è scritto sulla pietra, possiamo ampliarlo in seguito.  
Se noi stessi dovessimo scrivere la nostra storia non la narreremo certo nei dettagli, anno per anno. Diremmo solo le cose più importanti che hanno segnato la nostra vita.
#### Background sopra le righe
"Il mio personaggio era un arcimago che ha combattuto la grande guerra 1000 anni fa..." e ora per qualche ragione è un mago livello 4.  
Siamo realistici, cosa serve avere un personaggio che era un eroe diventato una persona comune? a colmare il proprio ego o per compensazione?  
Quando create il vostro personaggio, focalizzatevi sui suoi vizi, debolezze e aspetti oscuri. Questi elementi lo renderanno più interessante e coerente con la narrazione.
Badate non voglio limitare la vostra fantasia ma pensate, che elemento narrativo possiamo usare io o il mio master per fare crescere il mio personaggio.
#### Poca comunicazione con il master
Il BG andrebbe sempre fatto avere per tempo al master e validato da lui.
Questo serve per mantenere coerenza narrativa e giustificare l'inserimento nella storia.  
Un esempio banale è stata una campagna di *Sine Requie*, dove i poteri occulti sono rarissimi, il master ha deciso di limitarne il numero per preservare l’integrità dell’ambientazione. 
Ricordate inoltre che il master tutela l’ambientazione e la trama: un BG incoerente può essere accettato solo con il suo consenso. La decisione finale è sempre del narratore, non del giocatore.
## Il mio approccio
Come sempre la verità sta nel mezzo, Il mio approccio combina elementi schematici e narrativi del background.  
Io inizio elencando in modo schematico le informazioni fondamentali del personaggio come se compilassi un modulo prestampato.  
Successivamente descrivo in breve gli elementi fondamentali della sua vita, senza scendere in eccessivi dettagli.  
Infine in letteralmente due righe descrivo il carattere del personaggio.  
Tutto questo dovrebbe essere contenuto in una singola pagina.  
A questo punto prendo un evento significativo della vita del personaggio e lo **racconto** dal punto di vista del personaggio nel dettaglio. In questa stesura evidenzio i pensieri e il modo di parlare del personaggio, è come se faccessi una sessione zero di gioco con il personaggio in solitaria. Questa parte non ha una lunghezza predefinita, ma la cosa più importante è che deve essere totalmente opzionale, non deve contenere nulla che non sia presente sulla prima pagina.  
#### Vantaggi
Essendo il racconto opzionale può essere ignorato dal master, o questi può leggerlo per puro diletto.  
Essendo un racconto può concentrarsi come esercizio di stile dato che non deve contenere aspetti di gioco.  
Ti permette di concentrarti su aspetti interpretativi che solitamente i background non affrontano, quali il modo di parlare e pensare.  
È più piacevole da leggere e scrivere.  
#### Criticità
Richiede più tempo per scriverlo.
## Conclusione
Il background è una cosa vostra, il background perfetto non esiste, sperimentate e trovate ciò che rende il vostro personaggio vivo e unico, perché il viaggio è più importante della partenza