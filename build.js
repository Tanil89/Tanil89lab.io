const fs = require( 'node:fs/promises');
const path = require( 'node:path' );
const yaml = require( 'js-yaml' );
const fm = require('front-matter');

const config = require( './src/config.json' );
const sitemap = [`<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">`];


const naturalSort = function _s (a, b) {
    if ( a === b )
        return 0

    _s.intlSort = _s.intlSort || ( new Intl.Collator() ).compare

    const [a2, b2] = [a, b].map( s => s
        .replace( /[^\wÀ-ſ\s]+/g, '' )
        .toLowerCase()
    )
    if ( a2 !== b2 )
        return [a2, b2].sort( _s.intlSort )[0] === a2 ? -1 : 1

    return [a, b].sort( _s.intlSort )[0] === a ? -1 : 1
}

async function JSON_maker ( dir, pages) {
    const dir_content = await fs.readdir( dir )
    if ( dir_content ) {
        for (const file of dir_content) {
            const path = dir + '/' + file;
            if ( (await fs.lstat( path )).isFile()) {
                if ( file.endsWith(".md") || file.endsWith(".html") ) {
                    const yaml_data = await readYAML( path );
                    pages.push( [path, yaml_data] );
                }
            } else {
                await JSON_maker( path, pages );
            }
        }
    }
}

async function readYAML ( path ) {
    const text = await fs.readFile( path, 'utf8');
    const YAML = fm( text );
    return YAML;
}

function add_to_JSON(JSON_path, path_split, link, YAML) {
    let cursor = JSON_path
    for ( const key in path_split ) {
        const e = path_split[key].replace(".md", "").replace(".html", "");
        cursor = cursor.children
        cursor[e] = cursor[e] || {}
        cursor[e].children = cursor[e].children || {}
        cursor = cursor[e]
    }
    if ( Object.keys(cursor.children).length == 0 ) {
        delete cursor.children;
    }
    if ( YAML.body.trim().length > 0 ) {
        Object.assign(cursor, {link: link});
    }
    cursor.meta = {}
    Object.assign(cursor.meta, YAML.attributes);
}

;(async () => {
    try { 
        await fs.rm("dist", {recursive: true}) 
    } catch (e) {}
    await fs.mkdir("dist")
    await fs.cp("src", "dist", {recursive: true})

    const variants = {}
    for (const file of await fs.readdir( './src' )){
    //;(await fs.readdir( './src' )).forEach( async (file) => {
        const [_, name, lang] = file.match( /^pages_([^_]+)(?:_([^_]+))?$/ ) || []
        const test = await fs.lstat( './src/' + file )
        if ( (await fs.lstat( './src/' + file )).isDirectory() && name ) {
            variants[name] = {name, lang}
        }
    }
    
    const index = {_config: config }
    for (const v in variants) {
        const {name, lang} = variants[v]
        const directoryPath = `./src/pages_${name}_${lang}`;
        const jsonRoot = {variant: {name, lang}, meta: JSON.parse( await fs.readFile( directoryPath + "/meta.json", 'utf8' )), children: {}, search: []};
        let pages = []
        await JSON_maker(directoryPath, pages);
        pages = pages.sort( (a, b) => {
            const lenRoute = a[0].split('/').length - b[0].split('/').length
            if ( lenRoute ) return lenRoute;

            const index = ( a[1]?.attributes?.index ?? Number.MAX_VALUE ) - ( b[1]?.attributes?.index ?? Number.MAX_VALUE )
            if ( index ) return index;

            const date = ( a[1]?.attributes?.date ?? Number.MAX_VALUE ) - ( b[1]?.attributes?.date ?? Number.MAX_VALUE )
            if ( date ) return -date;

            if ( a[0] !== b[0] ) return naturalSort(a[0], b[0])[0] === a[0] ? -1 : 1
        })

        pages.forEach( ([path, yaml_data], i) => {
            // sitemap
            sitemap.push( `
                <url>
                    <loc>${path.replace(directoryPath, config.address + "/#!").replace(/\.\w+$/, '')}</loc>
                    ${yaml_data?.attributes?.date ? `<lastmod>${new Date(yaml_data?.attributes?.date).toISOString()}</lastmod>` : ''}
                </url>`
            );

            // json
            const link = path.replace("./src/", "")
            path_split = path.replace(directoryPath + "/", "").split( '/' );
            add_to_JSON( jsonRoot, path_split, link, yaml_data );

            // search list
            jsonRoot.search.push( {
                id: i,
                str: getPageName(yaml_data?.attributes) + ' ' + path_split.at(-1).replace(/\.\w+$/, '') + ' ' + Object.values( yaml_data?.attributes ).join(' ') + ' ' + path_split.slice(0, -1).reverse().join(' '),
                name: ( getPageName(yaml_data?.attributes) || path_split.at(-1)?.replace(/\.\w+$/, '').replace(/_/g, ' ' )) + (path_split.at(-2) ? (' (' + path_split.at(-2)?.replace(/\.\w+$/, '').replace(/_/g, ' ' ) + ')' ) : ''),
                url: path.replace( directoryPath + "/", "" ).replace(/\.\w+$/, '')
            })
        })
        
        index[name] = jsonRoot;
    }

    sitemap.push(`</urlset>`);
    fs.writeFile( "dist/index.json", JSON.stringify(index, null, "\t"), 'utf8');
    fs.writeFile( "dist/sitemap.xml", sitemap.join("\n"), 'utf8');
})();

function getPageName (page) {
    return page?.title || page?.name || ''
}