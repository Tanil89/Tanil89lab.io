---
title: L'importanza delle regole
author: Tanil
date: 2024-05-14T11:20:00+01:00
---
# L'importanza delle regole
## Perché abbiamo bisogno di regole?
> Il master è quell'amico che per farti star bene ti lascia entrare nei suoi sogni e ti da il permesso di lasciarci un segno

Il gioco di ruolo è questo: il master inventa un mondo e lascia la libertà ai giocatori di plasmarlo per scrivere una storia assieme a loro. Questo, da parte sua, è un atto di fiducia. Quindi, perché abbiamo bisogno di regole?

Una storia in cui non si incontrano ostacoli non è interessante da narrare, ma come gestire questi ostacoli?

Certo, il master potrebbe decidere arbitrariamente l'esito degli eventi. Questo è quello che di fatto accade nel "free form", dove il master decide ogni aspetto del gioco. Come fare a decidere se la spada del bandito ha colpito un punto vitale del giocatore oppure se il colpo non è stato fatale? Sembra una decisione da poco, ma riempie di molte responsabilità il Game Master. Se decidesse di graziare il giocatore, rischierebbe di togliere tutta la sfida e il pathos al gioco. Ho visto molti master graziare i giocatori perché non volevano che quel personaggio morisse, ma nonostante tutto, è un errore.

Il problema accade anche al contrario: vedere morire un personaggio a cui ci si era affezionati, quando il master aveva il potere di salvarlo, può creare astio all'interno del gruppo di gioco. Questo, dall'altra parte, è l'errore del giocatore. Il master dovrebbe essere un arbitro imparziale nella propria storia.

Da qui nasce la necessità di regole. Queste sollevano il Game Master dalla responsabilità di decidere su situazioni dubbie, lasciando il fardello al regolamento.
## Quando usare le regole?
Ora parlerò in maniera generale, in quanto ogni regolamento è a sé stante; tuttavia, ci sono regole generali che si applicano a qualsiasi regolamento. I test vanno usati in casi dove l'esito è **dubbio** oppure è interessante considerare il **grado di successo**. Chiariamo con qualche esempio:

Giochiamo in un mondo dove la magia esiste e tutti la possono incanalare. Il mago vuole provare a lanciare un trucchetto, una magia che conoscono tutti. Ha senso che faccia un test? Ha senso che possa fallire? No, perché quell'azione è semplice per lui e nessuno lo sta disturbando.
Ora la stessa azione la vuole fare il guerriero, dopo averla vista fare dal mago. Il master ha due scelte: potrebbe negargli il test in quanto non è un'abilità che potrebbe essere in grado di eseguire senza addestramento, oppure potrebbe far fare comunque il test con una difficoltà maggiore (ed un effetto peggiore della magia) se ritiene sia importante per la trama. Ad esempio, per permettere un multiclasse del personaggio oppure per introdurre aspetti fino ad allora non conosciuti.  
Il giocatore potrebbe sfruttare l'occasione di un test riuscito per acquistare un livello da mago, oppure dal lato del Game Master potrebbe introdurre la trama di un nuovo modo per imparare la magia in maniera non convenzionale.

Un altro esempio è un test di studio. Prendiamo ad esempio che lo studioso del gruppo provenga dalla capitale e chieda un test di studio sul monumento principale della città. Ha senso che fallisca? No, perché lo studio è il punto forte del suo personaggio, inoltre, è una conoscenza comune per il suo background. In questo caso, il test non è necessario **per quello specifico personaggio.**
Tuttavia, in questo caso, il Game Master potrebbe richiedere un test non per dare le informazioni, ma per aggiungere informazioni aggiuntive. Anche in presenza di un fallimento critico, il giocatore avrebbe le stesse informazioni che avrebbe avuto un giocatore normale, senza le sue conoscenze.

Solitamente il combattimento è sempre considerato un caso dubbio, in quanto la posta in gioco è maggiore; tuttavia, anche qui bisogna fare delle distinzioni.
Ricordo quando avevo iniziato a giocare, nella mia seconda sessione di D&D 4e, c'era l'ultimo nemico, un goblin prono nella palude con 2 punti vita. L'unico giocatore a portata ha dovuto attendere il suo turno per poter attaccare, fallendo l'attacco su un nemico che praticamente implorava di morire.
Morale della favola, abbiamo speso quasi 3 turni solo dietro a quel goblin per colpa dei dadi.
Col senno di poi, aveva senso proseguire quello scontro? No, perché non c'era più un grado di sfida, non c'era più interesse narrativo, non c'era più motivo di seguire le regole. Semplicemente era sufficiente interrompere lo scontro e decidere a tavolino come concluderlo.

Quello che secondo me bisogna evitare è trasformare il gioco in test continui, ogni giocatore dovrebbe scegliere l'approccio specifico, sebbene possa essere comico vedere il guerriero ignorante superare un test di studio su un argomento di cui non dovrebbe sapere nulla, può essere svilente per i personaggi che sono incentrati su quell'aspetto del gioco.
## Le regole dal punto di vista del giocatore
Secondo me, le regole sono tanto più importanti quanto è inesperto il giocatore. Ho giocato spesso con giocatori alle prime armi ed ho notato che molti di questi si bloccano non sapendo cosa possono o non possono fare.
Per questi giocatori è confortevole avere una serie di azioni codificate, fino a quando prendono confidenza con il gioco.
Successivamente, questi potranno liberamente pensare fuori dagli schemi, smettendo di pensare a "cosa _possono_ fare" a "cosa _vogliono_ fare".

Questo non vuol dire che il giocatore esperto ignori le regole; le regole sono lo scheletro su cui si basa il gioco, ma se un'azione non è codificata, non vuol dire che non si possa fare. Il giocatore è libero di esporre la propria idea al GM, che deciderà, in base al regolamento, se è possibile e come adattare le regole a tale scopo. Non limitate la fantasia.
## Il GM e le regole
Per un GM le regole sono degli impedimenti o validi strumenti? Dipende.
Il compito del GM è arbitrare il gioco; questo vuol dire che, oltre ad interagire con i propri PNG, il master interagisce con il giocatore attraverso il regolamento.
Ho sentito alcuni GM sentirsi oppressi dalle regole, tanto da cercare solo regolamenti molto narrativi. Mi viene in mente un mio caro amico che, se un regolamento ha una regola, questo ha una regola di troppo. Per me è il contrario: adoro regolamenti ben strutturati che comprendano molti casi.

Il regolamento permette di sgravare molte responsabilità dal punto di vista del GM. In fase preparatoria, il GM prevede i test principali con le possibili conseguenze e gli scontri; poi quello che accadrà non è più in suo potere. Questo dipende dalle scelte dei giocatori, comprese le conseguenze, come la morte di un personaggio.
Ho detto che i test vengono usati per definire le conseguenze, ma un GM accorto potrebbe usare un test per creare la trama in diretta!
Immaginate di poter sfruttare un critico su un test imprevisto per rivelare un personaggio importante della trama in anticipo, oppure per rivelare nuovi talenti. Questo può sembrare insignificante, ma per il giocatore è una cosa esaltante, che può addirittura spingere il giocatore a cambiare la crescita del suo personaggio.
Ricordo di avere fatto un successo critico in un test di studio, il master ha rivelato informazioni importanti e compromettenti su un nemico che non era ancora stato introdotto. Questo ha spinto il gruppo ad investigare su di lui, arrivando di fatto più preparati allo scontro. Il master ha confessato di non aver previsto la cosa ma di averla sfruttata. Le azioni dei giocatori sono importanti, non vanno trascurate.

### Il GM può infrangere le regole?
Come per il giocatore, il GM deve ricordare che il regolamento è una guida. Ma il GM può barare?
Sì, ma deve fare molta attenzione. Come ho detto prima, il regolamento tutela il Game Master, ma se decide di violarlo, lo fa a suo rischio e pericolo! Ecco alcune cose da tenere in conto:
* **Tutto il gruppo deve essere consapevole e d'accordo.**  
Ricordate che il regolamento è un'ancora per il giocatore; su di esso costruisce strategie e build. Comprare abilità inutili perché il master le ignora può essere frustrante.
* **Non deve essere una costante**  
Va bene "barare" qualche volta, ma deve essere situazionale e motivata.
* **Non deve essere fatto a vantaggio di qualcuno**  
Questi espedienti devono essere fatti per la trama e per l'atmosfera, non su una scelta strategica.

Quando volete piegare una regola, pensate: "Perché lo faccio? Sarei in grado di spiegarlo al giocatore finita la campagna?"  
Ricordo una volta che ho negato una magia con un test di magia riuscito a un giocatore. Il motivo era che un PNG presente in scena l'ha impedito. Il giocatore non era consapevole della presenza del PNG, per cui non gli ho fatto fare un test di percezione; l'ho fatto io segretamente per lui.  
Potevo semplicemente dire la verità sul counter spell? Certo, ma ho ritenuto che narrativamente fosse più interessante dire: "Qualcosa ti ha impedito di lanciare la magia". Giocavamo su un regolamento in cui tutti i test sono pubblici.

Il Game Master ha sempre l'ultima parola; è una grossa opportunità ma anche una grossa responsabilità, usatela bene!
## Tipi di regolamento
I regolamenti nei giochi di ruolo possono variare notevolmente in termini di approccio, spaziando da quelli più narrativi a quelli più meccanici.

Sistemi come _Dungeons & Dragons_ adottano un approccio simile a un gioco da tavolo, con regolamenti dettagliati che analizzano ogni aspetto del gioco. Ad alti livelli, un test può dover tenere conto di molti modificatori. Questa precisione nelle prove può ridurre la fluidità della narrazione. I combattimenti sono gestiti su una plancia, garantendo un aspetto tattico ma richiedendo molto lavoro per il GM.

Sistemi come _Fabula Ultima_ o _Ryuutama_ utilizzano regolamenti più minimali e versatili, dove i combattimenti possono essere tattici anche senza una plancia. Tuttavia, la scarsa specificità dei test, basati solo su statistiche senza modificatori, può ridurre la personalizzazione del personaggio, poiché la stessa statistica può essere usata per test di studio, percezione o orientamento.

Alcuni regolamenti sono specificamente narrativi, come _Not the End_ (che non ho provato). In questo gioco, la narrazione è fondamentale e i test introducono aspetti positivi e negativi in base ai successi e fallimenti estratti. Questo garantisce maggiore libertà narrativa, ma qualcuno potrebbe sentirsi forzato a introdurre aspetti narrativi solo perché sono stati estratti, ridicolizzando la storia.

### Regolamento punitivo o permissivo?
Non è direttamente legato all'aspetto meccanico, ma merita un'analisi. Ogni regolamento introduce delle difficoltà, ma alcuni sono molto punitivi, dove ogni scontro può essere letale.

In _Sine Requie_, i combattimenti sono molto pericolosi anche per personaggi di alto livello, spingendo i giocatori a evitare lo scontro. Ricordo che per un fallimento, il mio personaggio, nonostante fosse avanzato, stava per morire contro un non morto base. Questo mantiene alta la tensione anche nei combattimenti più semplici, ma può causare morti indecorose.

_Fabula Ultima_ punta a combattimenti meno punitivi; il personaggio non può morire se non decide di sacrificarsi. Questo permette di sperimentare la sconfitta senza ridurre il pathos.

Quando scegliete il gioco, considerate anche questo aspetto.
## Conclusioni
Il gioco di ruolo è un'esperienza libera e tale deve rimanere. Posso solo consigliare di scegliere accuratamente insieme un regolamento che si presti bene all'esperienza che volete affrontare, in modo da modificarlo il meno possibile.  
Non esiste solo D&D, esistono giochi più narrativi, più meccanici, più o meno punitivi, ecc. Provate, sperimentate e scegliete.

Personalmente, preferisco i giochi senza plancia. Ritengo che fare le mappe sia una quantità di lavoro esagerato come costo/risultato. Inoltre, percepisco molti combattimenti come un'interruzione al flusso della narrazione, quindi cerco sistemi che permettano di rendere emozionanti anche quelli.  
Quanto è bello avere un sistema senza classe armatura, con prove contrapposte? Ammetto che sono più lenti, ma fino alla narrazione del GM non sai mai cosa è successo, rimanendo sempre in trepidante attesa.

Cosa fate ancora qui? Scaldate i dadi!